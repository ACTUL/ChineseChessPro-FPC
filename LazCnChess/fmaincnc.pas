unit fmaincnc;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, Math, FPCanvas, LCLType, FPimage,StrUtils
  , BGRABitmap, bgrabitmaptypes
  ,ViewPiece,viewSesor,Board,Piece;

type

  { TFMain1 }

  TFMain1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    ImageBPlayer: TImage;
    ImageRPlayer: TImage;
    Memo1: TMemo;
    OpenDialog1: TOpenDialog;
    PanCeR: TPanel;
    PanCeB: TPanel;
    PanelBlackSide: TPanel;
    PanelRedSide: TPanel;
    SaveDialog1: TSaveDialog;
    Timer1: TTimer;
    ViewBoard: TImage;
    PanelL: TPanel;
    PanelM: TPanel;
    PanelR: TPanel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PanCeBClick(Sender: TObject);
    procedure PanCeBMouseLeave(Sender: TObject);
    procedure PanCeRClick(Sender: TObject);
    procedure PanCeRMouseLeave(Sender: TObject);
    procedure ViewBoardMouseLeave(Sender: TObject);
    procedure ViewBoardMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure ViewBoardClick(Sender: TObject);
    procedure ViewPcsBMouseEnter(Sender: TObject);
    procedure ViewPcsBMouseLeave(Sender: TObject);
    procedure ViewPcsRMouseEnter(Sender: TObject);
    procedure ViewPcsRMouseLeave(Sender: TObject);
    procedure ViewPcsBClick(Sender: TObject);
    procedure ViewPcsRClick(Sender: TObject);
    procedure ViewSesorClick(Sender: TObject);
    procedure PanelMEndDock(Sender, Target: TObject; X, Y: Integer);
    procedure PanelMResize(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    vnDL, vnDx, vnDy: Integer;
    //vnCx, vnCy: Integer;
    vIsReady:Boolean;
    vnStage:Integer; //场景状态:0=浏览棋局,1=编辑棋局
    //vFPCImg: TFPCustomImage;
    vViewSesor:TViewSesor;
    vnVBHintX,vnVBHintY:Integer;
    vBoard:TBoard;
    procedure FreePcsLst;
    procedure MakePcsLst;
    procedure PaintBoard;
    procedure PaintBoardGrid;
    procedure ReDrawPcsLst;
    procedure ReDrawViewBoard;
    procedure ReSizeProc;
  protected
    procedure PutPcsToBoxCells;
    procedure pShowBoard(aBoard:TBoard);

  public
    vPcsLstB:TStringList;
    vPcsLstR:TStringList;
    vGridXScales:array[0..8] of Integer; //X 刻度 at board
    vGridYScales:array[0..9] of Integer; //Y 刻度 at board
    procedure printBoardH(aBoard:TBoard);
    procedure viewToBoard(aBoard:TBoard);
  end;

var
  FMain1: TFMain1;

implementation
const OriPcsPosRC:array[0..15] of char =  ('A','A','B','B','C','C','D','D','E','F','F','G','G','G','G','G');
const OriPcsPosBC:array[0..15] of char =  ('H','H','I','I','J','J','K','K','L','M','M','N','N','N','N','N');

{$R *.lfm}

{ TFMain1 }

procedure TFMain1.Button1Click(Sender: TObject);
begin
  PaintBoard;
  ImageBPlayer.Canvas.Ellipse(1, 1, 42, 42);
  ImageRPlayer.Canvas.Brush.color := clRed;
  ImageRPlayer.Canvas.FillRect(ViewBoard.Canvas.ClipRect);
  ImageRPlayer.Canvas.Ellipse(1, 1, 42, 42);

  //ImgP1.Transparent:=False;
  //ImgP1.Picture.Clear;
  //ImgP1.Width := vnDL;
  //ImgP1.Height := vnDL;
  //if (ImgP1.Picture<>nil)and(ImgP1.Picture.Bitmap<>nil) then
  //begin
  //ImgP1.Picture.Bitmap.Width:=ImgP1.Width;
  //ImgP1.Picture.Bitmap.Height:=ImgP1.Height;
  //end;
  memo1.Lines.Add(IntToStr(vnDL));
  Self.vnStage:=0; //浏览状态
  Self.vViewSesor.Selected:=False;
  Self.vViewSesor.Visible:=False;
end;

procedure TFMain1.Button2Click(Sender: TObject);
begin
  PutPcsToBoxCells;
end;

procedure TFMain1.Button3Click(Sender: TObject);
begin
  Self.vnStage:=1; //自由编辑
  Self.vViewSesor.BringToFront;
end;

procedure TFMain1.Button4Click(Sender: TObject);
begin
  TBoard.initBoardStd(vBoard);
  printBoardH(vBoard);
  pShowBoard(vBoard);
end;

procedure TFMain1.Button5Click(Sender: TObject);
var sFileName:string;
  aTxtF:TextFile;
  sLin:string;
  nX,nY:Byte;
begin
  viewToBoard(Self.vBoard);
  printBoardH(vBoard);
  SaveDialog1.Filter:='ChineseChessBoard file|*.ccb';
  if SaveDialog1.Execute then
  begin
    sFileName:=SaveDialog1.FileName;
    AssignFile(aTxtF,sFileName);
    try
      ReWrite(aTxtF);
      for nY:=Board.cYmax downto 0 do
      begin
        sLin:='';
        for nX:=Board.cXmax downto 0 do
        begin
          sLin:=sLin+vBoard.points[nX,nY].code;
        end;
        WriteLn(aTxtF,sLin);
      end;
    finally
      CloseFile(aTxtF);
    end;
  end;
end;

procedure TFMain1.Button6Click(Sender: TObject);
var sFileName:string;
  aTxtF:TextFile;
  sLin:AnsiString;
  nX,nY:SmallInt;
  aIc,aILen,aIv,aIx:Integer;
  aCode,aSide,aSub:AnsiChar;
  aVwPcs:TViewPiece;
begin
  TBoard.clearBoard(vBoard);
  OpenDialog1.Filter:='ChineseChessBoard file|*.ccb';
  if OpenDialog1.Execute then
  begin
    sFileName:=OpenDialog1.FileName;
   if FileExists(sFileName) then
   begin
    aIc:=vPcsLstB.Count;
    for aIv:=0 to aIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstB.Objects[aIv]);
      aVwPcs.isOnBoard:=False;
    end;
    aIc:=vPcsLstR.Count;
    for aIv:=0 to aIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstR.Objects[aIv]);
      aVwPcs.isOnBoard:=False;
    end;
    //Memo1.Clear;
    AssignFile(aTxtF,sFileName);
    try
      Reset(aTxtF);
      nY:=Board.cYmax;
      while not Eof(aTxtF) do
      begin
        ReadLn(aTxtF,sLin);
        aILen:=Length(sLin);
        nX:=Board.cXmax;
        for aIx:=1 to aILen do
        begin
          aCode:=sLin[aIx];
          if (nX>=0)and(nY>=0) then
          begin
            aSide:=getSide(aCode);
            //Memo1.Lines.Add('nY:'+IntToStr(nY)+',nX:'+IntToStr(nX)+',aSide:'+aSide+',aCode:'+aCode);
            if (aSide='B') then
            begin
              vBoard.points[nX,nY].code:=aCode;
              for aIv:=0 to aIc-1 do
              begin
                aVwPcs:=TViewPiece(vPcsLstB.Objects[aIv]);
                if (aVwPcs.isOnBoard=False)and(aVwPcs.Code=aCode) then
                begin
                  aVwPcs.isOnBoard:=True;
                  if (Length(aVwPcs.AID)>1) then
                  begin
                  aSub:=aVwPcs.AID[2];
                  vBoard.points[nX,nY].sub:=aSub;
                  //Memo1.Lines.Add('Has Sub:'+aSub);
                  end else
                  begin
                    //Memo1.Lines.Add('Has No Sub.');
                  end;
                  Break;
                end;
              end;
            end else if (aSide='R') then
            begin
              vBoard.points[nX,nY].code:=aCode;
              for aIv:=0 to aIc-1 do
              begin
                aVwPcs:=TViewPiece(vPcsLstR.Objects[aIv]);
                if (aVwPcs.isOnBoard=False)and(aVwPcs.Code=aCode) then
                begin
                  aVwPcs.isOnBoard:=True;
                  if (Length(aVwPcs.AID)>1) then
                  begin
                  aSub:=aVwPcs.AID[2];
                  vBoard.points[nX,nY].sub:=aSub;
                  //Memo1.Lines.Add('Has Sub:'+aSub);
                  end else
                  begin
                    //Memo1.Lines.Add('Has No Sub.');
                  end;
                  Break;
                end;
              end;
            end;
          end;
          if (nX<=0) then Break;
          Dec(nX);
        end;
        if (nY<=0) then Break;
        Dec(nY);
      end;
    finally
      CloseFile(aTxtF);
    end;
   end;
  end;
    printBoardH(vBoard);
    PutPcsToBoxCells;
    pShowBoard(vBoard);
end;

procedure TFMain1.FormCreate(Sender: TObject);
begin
    vPcsLstB:=TStringList.Create;
    vPcsLstR:=TStringList.Create;
    vIsReady:=False;
    vViewSesor:=TViewSesor.Create(nil);
    vViewSeSor.OnClick:=@ViewSesorClick;
    vnVBHintX:=-1;
    vnVBHintY:=-1;
    ViewBoard.ShowHint:=False;

    vBoard:=TBoard.Create;

end;

procedure TFMain1.FormDestroy(Sender: TObject);
begin
    FreePcsLst;
    vPcsLstB.Free;
    vPcsLstR.Free;
    vViewSesor.Free;
end;

procedure TFMain1.FormResize(Sender: TObject);
begin
  Memo1.Lines.Add('FormResize');
  Timer1.Enabled:=True;
end;

procedure TFMain1.FormShow(Sender: TObject);
begin

  PaintBoard;
  if vPcsLstB.Count=0 then
     MakePcsLst;
  vIsReady:=True;
  ReDrawPcsLst;
  PutPcsToBoxCells;
end;

procedure TFMain1.PanCeBClick(Sender: TObject);
var sID:string;
var aVwPcs:TViewPiece;
  nIx,nIc:Integer;
begin
  //
  if Self.vnStage=1 then
  begin
    if vViewSesor.selected and vViewSesor.Visible and (vViewSesor.SeleS='B')
    and (vViewSesor.Parent=PanelM) then
    begin
      sID:=vViewSesor.SeleID;
      nIx:=vPcsLstB.IndexOf(SID);
      if nIx>=0 then
      begin
        aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
        aVwPcs.Parent:=PanCeB;
        aVwPcs.Left:=aVwPcs.inL;
        aVwPcs.Top :=aVwPcs.inT;
        aVwPcs.isOnBoard:=False;
        vViewSesor.selected:=False;
      end;
    end;

  end;
end;

procedure TFMain1.PanCeBMouseLeave(Sender: TObject);
begin
  if not vViewSesor.selected then
  if vViewSesor.Visible then
     vViewSesor.Visible:=False;
end;

procedure TFMain1.PanCeRClick(Sender: TObject);
var sID:string;
var aVwPcs:TViewPiece;
  nIx,nIc:Integer;
begin
  //
  if Self.vnStage=1 then
  begin
    if vViewSesor.selected and vViewSesor.Visible and (vViewSesor.SeleS='R')
    and (vViewSesor.Parent=PanelM) then
    begin
      sID:=vViewSesor.SeleID;
      nIx:=vPcsLstR.IndexOf(SID);
      if nIx>=0 then
      begin
        aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
        aVwPcs.Parent:=PanCeR;
        aVwPcs.Left:=aVwPcs.inL;
        aVwPcs.Top :=aVwPcs.inT;
        aVwPcs.isOnBoard:=False;
        vViewSesor.selected:=False;
      end;
    end;

  end;   end;

procedure TFMain1.PanCeRMouseLeave(Sender: TObject);
begin
  if not vViewSesor.selected then
  if vViewSesor.Visible then
     vViewSesor.Visible:=False;
end;

procedure TFMain1.ViewBoardMouseLeave(Sender: TObject);
var x1,y1,x2,y2:Integer;
  aPenMode:TPenMode;
  aColr:TColor;
  aStyle:TFPPenStyle;
  apw:Integer;
  delta:Integer;
begin
  if not vIsReady then Exit;
  delta:=8;
  if (ViewBoard.ShowHint) then
  begin
    aPenMode:=ViewBoard.Canvas.Pen.Mode;
    aColr:=ViewBoard.Canvas.Pen.Color;
    aStyle:= ViewBoard.Canvas.Pen.Style;
    apw:= ViewBoard.Canvas.Pen.Width;
    ViewBoard.ShowHint:=False;
    x1:=(vnVBHintX+0)*vnDL-delta+vnDx;
    y1:=(vnVBHintY+0)*vnDL-delta+vnDy;
    x2:=(vnVBHintX+0)*vnDL+delta+vnDx;
    y2:=(vnVBHintY+0)*vnDL+delta+vnDy;
    ViewBoard.Canvas.Pen.Color:=clYellow;
    ViewBoard.Canvas.Pen.Width:=2;
    ViewBoard.Canvas.Pen.Style:=psSolid;
    ViewBoard.Canvas.Pen.Mode:=pmXor;
    ViewBoard.Canvas.Frame(x1,y1,x2,y2);
    //ViewBoard.Canvas.Rectangle(x1,y1,x2,y2);
    ViewBoard.Canvas.Pen.Color:=aColr;
    ViewBoard.Canvas.Pen.Style:=aStyle;
    ViewBoard.Canvas.Pen.Mode:= aPenMode;
    ViewBoard.Canvas.Pen.Width:= apw;
    Memo1.Lines.add('ViewBoardMouseLeave. ClearFrame');
  end;
end;

procedure TFMain1.ViewBoardMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var nX,nY:Integer;
    rX,rY:Real;
    x1,y1,x2,y2:Integer;
    aPenMode:TPenMode;
    aColr:TColor;
    aStyle:TFPPenStyle;
    apw:Integer;
    delta:Integer;

begin
  if not vIsReady then Exit;
    delta:=8;
  rX:=(X-vnDx) / vnDL;
  rY:=(Y-vnDy) / vnDL;
  nX:=Trunc(rX+0.5);
  nY:=Trunc(rY+0.5);
  if (nX>=0)and(nX<=8)and(nY>=0)and(nY<=9) then
  begin
    aPenMode:=ViewBoard.Canvas.Pen.Mode;
    aColr:=ViewBoard.Canvas.Pen.Color;
    aStyle:= ViewBoard.Canvas.Pen.Style;
    apw:= ViewBoard.Canvas.Pen.Width;
    ViewBoard.Canvas.Pen.Color:=clYellow;
    ViewBoard.Canvas.Pen.Width:=2;
    ViewBoard.Canvas.Pen.Style:=psSolid;
    ViewBoard.Canvas.Pen.Mode:=pmXor;
    if (ViewBoard.ShowHint)and((nX<>vnVBHintX)or(nY<>vnVBHintY)) then
    begin
      x1:=(vnVBHintX+0)*vnDL-delta+vnDx;
      y1:=(vnVBHintY+0)*vnDL-delta+vnDy;
      x2:=(vnVBHintX+0)*vnDL+delta+vnDx;
      y2:=(vnVBHintY+0)*vnDL+delta+vnDy;
      ViewBoard.Canvas.Frame(x1,y1,x2,y2);
      //ViewBoard.Canvas.Rectangle(x1,y1,x2,y2);
      Memo1.Lines.add('ViewBoardMouseMove. ClearFrame');
      ViewBoard.ShowHint:=False;
    end;
    if (not ViewBoard.ShowHint) then
    begin
    x1:=(nX+0)*vnDL-delta+vnDx;
    y1:=(nY+0)*vnDL-delta+vnDy;
    x2:=(nX+0)*vnDL+delta+vnDx;
    y2:=(nY+0)*vnDL+delta+vnDy;
    ViewBoard.Canvas.Frame(x1,y1,x2,y2);
    //ViewBoard.Canvas.Rectangle(x1,y1,x2,y2);
    Memo1.Lines.add('ViewBoardMouseMove. ShowFrame');
    vnVBHintX:=nX;
    vnVBHintY:=nY;
    ViewBoard.ShowHint:=True;
    end;
    ViewBoard.Canvas.Pen.Color:=aColr;
    ViewBoard.Canvas.Pen.Style:=aStyle;
    ViewBoard.Canvas.Pen.Mode:= aPenMode;
    ViewBoard.Canvas.Pen.Width:= apw;
  end;
end;

procedure TFMain1.ViewBoardClick(Sender: TObject);
var aVwPcs:TViewPiece;
    sID:string;
    nIx,nSx,nSy:Integer;
    aPc,aPs:TPoint;
begin
  if vnStage=1 then //自由编辑
  begin
  if Self.vViewSesor.Selected and ViewBoard.ShowHint then
  begin
    Memo1.Lines.Add('ViewBoardClick,ShowHint');
    aVwPcs:=nil;
    sID:=vViewSesor.SeleID;
    if (vViewSesor.SeleS='B') then
    begin
      nIx:=vPcsLstB.IndexOf(sID);
      Memo1.Lines.Add('ViewBoardClick,vPcsLstB.Index:'+IntToStr(nIx));
      if (nIx>=0) then
      begin
        aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
      end;
    end else
    begin
      nIx:=vPcsLstR.IndexOf(sID);
      Memo1.Lines.Add('ViewBoardClick,vPcsLstR.Index:'+IntToStr(nIx));
      if (nIx>=0) then
      begin
        aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
      end;
    end;
    if (aVwPcs<>nil) then
    begin
      Memo1.Lines.Add('ViewBoardClick,aVwPcs<>nil.');
      aVwPcs.Parent:=PanelM;
      nSx:=  vnVBHintX*vnDL + vnDx - vnDL div 2;
      nSy:=  vnVBHintY*vnDL + vnDy - vnDL div 2;
      aPc:=Point(nSx,nSy);
      aPs:=  ViewBoard.ClientToScreen(aPc);
      aPc:=  PanelM.ScreenToClient(aPs);
      aVwPcs.Left := aPc.x;
      aVwPcs.Top  := aPc.y;
      aVwPcs.inI :=vnVBHintX;
      aVwPcs.inJ :=vnVBHintY;
      aVwPcs.isOnBoard:=True;
      //vViewSesor.Selected:=False;
    end;
  end;
  end;
end;

procedure TFMain1.ViewPcsBMouseEnter(Sender: TObject);
var aVwPcs:TViewPiece;
    aPenMode:TPenMode;
    aRect:TRect;
    aColr:TColor;
    aStyle:TFPPenStyle;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
  if Sender is  TViewPiece then
  begin
    aVwPcs := TViewPiece(Sender);
    {
    aPenMode:=aVwPcs.Canvas.Pen.Mode;
    aColr:=aVwPcs.Canvas.Pen.Color;
    aStyle:= aVwPcs.Canvas.Pen.Style;
    aVwPcs.Canvas.Pen.Color:=clYellow;
    aVwPcs.Canvas.Pen.Mode:=pmXor;
    //aVwPcs.Canvas.Pen.Style:=psDash;
    aVwPcs.Canvas.Pen.Width:=3;
    aRect.Left:=aVwPcs.Width div 3;
    aRect.Top:=aVwPcs.Width div 3;
    aRect.Right :=aVwPcs.Width *2 div 3;
    aRect.Bottom:=aVwPcs.Width *2 div 3;
    //aVwPcs.Canvas.DrawFocusRect(aRect);
    //aVwPcs.Canvas.Rectangle(aRect);
    aVwPcs.Canvas.Pen.Color:=aColr;
    aVwPcs.Canvas.Pen.Style:=aStyle;
    aVwPcs.Canvas.Pen.Mode:= aPenMode;
    memo1.Lines.Add('ViewPcsBMouseEnter,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    }
    //if (aVwPcs.Parent = PanCeB) then
    //begin
      if not vViewSesor.Selected then
      begin
        if vViewSesor.Parent<>aVwPcs.Parent then
           vViewSesor.Parent:=aVwPcs.Parent;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS :='B';
        memo1.Lines.Add('ViewPcsBMouseEnter,not vViewSesor.Selected,AID:'+vViewSesor.SeleID);
      end;
      if not vViewSesor.Visible then
         vViewSesor.Visible:=True;
    //end;
  end else
  begin
    memo1.Lines.Add('ViewPcsBMouseEnter');
  end;
  end;
end;

procedure TFMain1.ViewPcsBMouseLeave(Sender: TObject);
var aVwPcs:TViewPiece;
    aPenMode:TPenMode;
    aRect:TRect;
    aColr:TColor;
    aStyle:TFPPenStyle;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
  if Sender is  TViewPiece then
  begin
    aVwPcs := TViewPiece(Sender);
    {
    aPenMode:=aVwPcs.Canvas.Pen.Mode;
    aColr:=aVwPcs.Canvas.Pen.Color;
    aStyle:= aVwPcs.Canvas.Pen.Style;
    aVwPcs.Canvas.Pen.Color:=clYellow;
    aVwPcs.Canvas.Pen.Mode:=pmXor;
    //aVwPcs.Canvas.Pen.Style:=psDash;
    aVwPcs.Canvas.Pen.Width:=3;
    aRect.Left:=aVwPcs.Width div 3;
    aRect.Top:=aVwPcs.Width div 3;
    aRect.Right :=aVwPcs.Width *2 div 3;
    aRect.Bottom:=aVwPcs.Height *2 div 3;
    //aVwPcs.Canvas.DrawFocusRect(aRect);
    //aVwPcs.Canvas.Rectangle(aRect);
    aVwPcs.Canvas.Pen.Color:=aColr;
    aVwPcs.Canvas.Pen.Style:=aStyle;
    aVwPcs.Canvas.Pen.Mode:= aPenMode;
    memo1.Lines.Add('ViewPcsBMouseLeave,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    }
    if vViewSesor.Selected and (vViewSesor.SeleID=aVwPcs.AID)
      and ( (Abs(vViewSesor.Left-aVwPcs.Left)>24) or (Abs(vViewSesor.Top-aVwPcs.Top)>24) ) then
    begin
      if vViewSesor.Selected then
        vViewSesor.Selected:=False;
    end;
  end else
  begin
    memo1.Lines.Add('ViewPcsBMouseLeave');
  end;
  end;
end;

procedure TFMain1.ViewPcsRMouseEnter(Sender: TObject);
var aVwPcs:TViewPiece;
    aPenMode:TPenMode;
    aRect:TRect;
    aColr:TColor;
    aStyle:TFPPenStyle;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
  if Sender is  TViewPiece then
  begin
    aVwPcs := TViewPiece(Sender);
    {
    aPenMode:=aVwPcs.Canvas.Pen.Mode;
    aColr:=aVwPcs.Canvas.Pen.Color;
    aStyle:= aVwPcs.Canvas.Pen.Style;
    aVwPcs.Canvas.Pen.Color:=clYellow;
    aVwPcs.Canvas.Pen.Mode:=pmXor;
    //aVwPcs.Canvas.Pen.Style:=psDash;
    aVwPcs.Canvas.Pen.Width:=3;
    aRect.Left:=aVwPcs.Width div 3;
    aRect.Top:=aVwPcs.Width div 3;
    aRect.Right :=aVwPcs.Width *2 div 3;
    aRect.Bottom:=aVwPcs.Width *2 div 3;
    //aVwPcs.Canvas.DrawFocusRect(aRect);
    //aVwPcs.Canvas.Rectangle(aRect);
    aVwPcs.Canvas.Pen.Color:=aColr;
    aVwPcs.Canvas.Pen.Style:=aStyle;
    aVwPcs.Canvas.Pen.Mode:= aPenMode;
    memo1.Lines.Add('ViewPcsBMouseEnter,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    }
    //if (aVwPcs.Parent = PanCeR) then
    //begin
      if not vViewSesor.Selected then
      begin
        if vViewSesor.Parent<>aVwPcs.Parent then
           vViewSesor.Parent:=aVwPcs.Parent;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS :='R';
        memo1.Lines.Add('ViewPcsRMouseEnter,not vViewSesor.Selected,AID:'+vViewSesor.SeleID);
      end;
      if not vViewSesor.Visible then
         vViewSesor.Visible:=True;
    //end;
  end else
  begin
    memo1.Lines.Add('ViewPcsRMouseEnter');
  end;
  end;
end;

procedure TFMain1.ViewPcsRMouseLeave(Sender: TObject);
var aVwPcs:TViewPiece;
    aPenMode:TPenMode;
    aRect:TRect;
    aColr:TColor;
    aStyle:TFPPenStyle;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
  if Sender is  TViewPiece then
  begin
    aVwPcs := TViewPiece(Sender);
  {
    aPenMode:=aVwPcs.Canvas.Pen.Mode;
    aColr:=aVwPcs.Canvas.Pen.Color;
    aStyle:= aVwPcs.Canvas.Pen.Style;
    aVwPcs.Canvas.Pen.Color:=clYellow;
    aVwPcs.Canvas.Pen.Mode:=pmXor;
    //aVwPcs.Canvas.Pen.Style:=psDash;
    aVwPcs.Canvas.Pen.Width:=3;
    aRect.Left:=aVwPcs.Width div 3;
    aRect.Top:=aVwPcs.Width div 3;
    aRect.Right :=aVwPcs.Width *2 div 3;
    aRect.Bottom:=aVwPcs.Height *2 div 3;
    //aVwPcs.Canvas.DrawFocusRect(aRect);
    //aVwPcs.Canvas.Rectangle(aRect);
    aVwPcs.Canvas.Pen.Color:=aColr;
    aVwPcs.Canvas.Pen.Style:=aStyle;
    aVwPcs.Canvas.Pen.Mode:= aPenMode;
    memo1.Lines.Add('ViewPcsBMouseLeave,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    }
    if vViewSesor.Selected and (vViewSesor.SeleID=aVwPcs.AID)
      and ( (Abs(vViewSesor.Left-aVwPcs.Left)>24) or (Abs(vViewSesor.Top-aVwPcs.Top)>24) ) then
    begin
      if vViewSesor.Selected then
        vViewSesor.Selected:=False;
    end;
  end else
  begin
    memo1.Lines.Add('ViewPcsRMouseLeave');
  end;
  end;
end;

procedure TFMain1.ViewPcsBClick(Sender: TObject);
var aVwPcs:TViewPiece;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
    if Sender is  TViewPiece then
    begin
      aVwPcs := TViewPiece(Sender);
      //if (not vViewSesor.Selected) and (vViewSesor.Visible) and (Abs(vViewSesor.Left-aVwPcs.Left)<12) and (Abs(vViewSesor.Top-aVwPcs.Top)<12) then
      //begin
      //  vViewSesor.Selected:=True;
      //end;
      if vViewSesor.Selected and (vViewSesor.SeleID=aVwPcs.AID) then
      begin
        vViewSesor.Selected:=False;
        vViewSesor.SeleID:='';
        vViewSesor.SeleS:=' ';
        memo1.Lines.Add('ViewPcsBClick,vViewSesor.SeleID=aVwPcs.AID');
      end else if vViewSesor.Selected and (vViewSesor.SeleID<>aVwPcs.AID) then
      begin
        {
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS:='B';
        //vViewSesor.Parent:=PanCeB;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        }
        memo1.Lines.Add('ViewPcsBClick,vViewSesor.SeleID<>aVwPcs.AID');
      end else
      begin
        vViewSesor.Selected:=True;
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS:='B';
        //vViewSesor.Parent:=PanCeB;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        memo1.Lines.Add('ViewPcsBClick,Sesor.Sele:=True,AID:'+vViewSesor.SeleID);
      end;
      if (vViewSesor.Selected) then
      if (vViewSesor.Parent<>aVwPcs.Parent) then
        vViewSesor.Parent:=aVwPcs.Parent;
      memo1.Lines.Add('ViewPcsBClick,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    end else
    begin
      memo1.Lines.Add('ViewPcsBClick');
    end;
  end;
end;

procedure TFMain1.ViewPcsRClick(Sender: TObject);
var aVwPcs:TViewPiece;
begin
  if not vIsReady then Exit;
  if vnStage=1 then //自由编辑
  begin
    if Sender is  TViewPiece then
    begin
      aVwPcs := TViewPiece(Sender);
      if vViewSesor.Selected and (vViewSesor.SeleID=aVwPcs.AID) then
      begin
        vViewSesor.Selected:=False;
        vViewSesor.SeleID:='';
        vViewSesor.SeleS:=' ';
      end else if vViewSesor.Selected and (vViewSesor.SeleID<>aVwPcs.AID) then
      begin
        {
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS:='R';
        //vViewSesor.Parent:=PanCeR;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        }
        memo1.Lines.Add('ViewPcsBClick,vViewSesor.SeleID<>aVwPcs.AID');
      end else
      begin
        vViewSesor.Selected:=True;
        vViewSesor.SeleID:=aVwPcs.AID;
        vViewSesor.SeleS:='R';
        //vViewSesor.Parent:=PanCeR;
        vViewSesor.Left:=aVwPcs.Left;
        vViewSesor.Top:=aVwPcs.Top;
        memo1.Lines.Add('ViewPcsRClick,Selected:=True,AID:'+vViewSesor.SeleID);
      end;
      if (vViewSesor.Selected) then
      if (vViewSesor.Parent<>aVwPcs.Parent) then
        vViewSesor.Parent:=aVwPcs.Parent;

      memo1.Lines.Add('ViewPcsRClick,CName:'+aVwPcs.CName+',Left:'+IntToStr(aVwPcs.Left)+',Top:'+IntToStr(aVwPcs.Top));
    end else
    begin
      memo1.Lines.Add('ViewPcsRClick');
    end;
  end;
end;

procedure TFMain1.ViewSesorClick(Sender: TObject);
begin
  vViewSesor.Selected:=not vViewSesor.Selected;
  memo1.Lines.Add('ViewSesorClick,vViewSesor.Selected:'+BoolToStr(vViewSesor.Selected));
end;

procedure TFMain1.PanelMEndDock(Sender, Target: TObject; X, Y: Integer);
begin
  memo1.Lines.Add('PanelMEndDock');
end;


procedure TFMain1.PanelMResize(Sender: TObject);
begin
  //Memo1.Lines.Add('PanelMResize');
end;

procedure TFMain1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled:=False;
  ReSizeProc;
end;

procedure TFMain1.FreePcsLst;
begin
  while vPcsLstB.Count>0 do
  begin
    if (vPcsLstB.Objects[vPcsLstB.Count-1] <> nil) then
      vPcsLstB.Objects[vPcsLstB.Count-1].Free;
    vPcsLstB.Delete(vPcsLstB.Count-1);
  end;
  while vPcsLstR.Count>0 do
  begin
    if (vPcsLstR.Objects[vPcsLstR.Count-1] <> nil) then
      vPcsLstR.Objects[vPcsLstR.Count-1].Free;
    vPcsLstR.Delete(vPcsLstR.Count-1);
  end;
end;

procedure TFMain1.MakePcsLst;
var aVwPcs:TViewPiece;
begin
    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRA_Ju+'1';
    aVwPcs.Code:=cRA_Ju;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'车';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRA_Ju+'2';
    aVwPcs.Code:=cRA_Ju;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'车';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRB_Ma+'1';
    aVwPcs.Code:=cRB_Ma;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'马';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRB_Ma+'2';
    aVwPcs.Code:=cRB_Ma;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'马';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRC_Xng+'1';
    aVwPcs.Code:=cRC_Xng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'相';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRC_Xng+'2';
    aVwPcs.Code:=cRC_Xng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'相';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRD_Shi+'1';
    aVwPcs.Code:=cRD_Shi;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'仕';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRD_Shi+'2';
    aVwPcs.Code:=cRD_Shi;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'仕';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRE_Jng+'1';
    aVwPcs.Code:=cRE_Jng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'帥';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRF_Pao+'1';
    aVwPcs.Code:=cRF_Pao;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'炮';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRF_Pao+'2';
    aVwPcs.Code:=cRF_Pao;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'炮';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRG_Zu+'1';
    aVwPcs.Code:=cRG_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'兵';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRG_Zu+'2';
    aVwPcs.Code:=cRG_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'兵';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRG_Zu+'3';
    aVwPcs.Code:=cRG_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'兵';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRG_Zu+'4';
    aVwPcs.Code:=cRG_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'兵';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cRG_Zu+'5';
    aVwPcs.Code:=cRG_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'兵';
    aVwPcs.Side:=cSideRed;
    vPcsLstR.AddObject(aVwPcs.AID,aVwPcs);


    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBH_Ju+'1';
    aVwPcs.Code:=cBH_Ju;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'車';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBH_Ju+'2';
    aVwPcs.Code:=cBH_Ju;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'車';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBI_Ma+'1';
    aVwPcs.Code:=cBI_Ma;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'馬';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBI_Ma+'2';
    aVwPcs.Code:=cBI_Ma;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'馬';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBJ_Xng+'1';
    aVwPcs.Code:=cBJ_Xng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'象';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBJ_Xng+'2';
    aVwPcs.Code:=cBJ_Xng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'象';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBK_Shi+'1';
    aVwPcs.Code:=cBK_Shi;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'士';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBK_Shi+'2';
    aVwPcs.Code:=cBK_Shi;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'士';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBL_Jng+'1';
    aVwPcs.Code:=cBL_Jng;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'將';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBM_Pao+'1';
    aVwPcs.Code:=cBM_Pao;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'砲';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBM_Pao+'2';
    aVwPcs.Code:=cBM_Pao;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'砲';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBN_Zu+'1';
    aVwPcs.Code:=cBN_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'卒';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBN_Zu+'2';
    aVwPcs.Code:=cBN_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'卒';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBN_Zu+'3';
    aVwPcs.Code:=cBN_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'卒';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBN_Zu+'4';
    aVwPcs.Code:=cBN_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'卒';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);

    aVwPcs:=TViewPiece.Create(nil);
    aVwPcs.DLen:=Self.vnDL;
    aVwPcs.AID:=cBN_Zu+'5';
    aVwPcs.Code:=cBN_Zu;
    aVwPcs.CName:=Piece.getName(aVwPcs.Code);//'卒';
    aVwPcs.Side:=cSideBlk;
    vPcsLstB.AddObject(aVwPcs.AID,aVwPcs);



end;

procedure TFMain1.PaintBoard;
var
  aRect: TRect;
  dy, xr, yr: real;
  nH, nW, nx2, nx, ny, ny1, ny2: integer;
  nJ, nK, nHd, nYR: integer;
  aColor0: TColor;
  aColor1: TColor;
  aColor2: TColor;
  aColorx: TColor;
  arYs: array of integer;
  aRGB: longint;
  b_A, b_B, b_G, b_R, bCR: byte;
begin
  //aRect:=Rect(22,33,88,99);
  aColor0 := cBackColor; //$004AB967;
  aColor1 := aColor0;
  ViewBoard.Canvas.Brush.color := aColor1;
  //ViewBoard.Canvas.FillRect(ViewBoard.Canvas.ClipRect);
  ViewBoard.Canvas.FillRect(ViewBoard.ClientRect);
  //ViewBoard.Canvas.GradientFill(aRect,$004AD967,$004A99A7,gdHorizontal);
  //ViewBoard.Canvas.Ellipse(1,5,55,77);
  nW := ViewBoard.Width;
  nH := ViewBoard.Height;
  ViewBoard.Canvas.Pen.color := clBlack;
  nx2 := nW div 2;
  nHd := 20;
  SetLength(arYs, nx2);
  for nx := 0 to nx2 - 1 do
    arYs[nx] := 0;
  Randomize;
  for nJ := 0 to 9 do
  begin
    nx := 0;
    //yr:= Math.Power((nx - nx2 div 2),2)/(nHd-nJ*2);
    dy := -100 + nJ * 50;
    //ny:=Trunc(yr+dy);
    //ViewBoard.Canvas.MoveTo(nx,ny);
    //aRGB:=ColorToRGB(aColor1);
    aRGB := longint(aColor1);
    b_B := (aRGB and $FF0000) shr 16;
    b_G := (aRGB and $FF00) shr 8;
    b_R := (aRGB and $FF);
    bCR := 1;//-Random(2);
    if (bCR = 0) then
      bCR := 1;
    aColor2 := RGBToColor(Trunc(b_R * bCR * 1.03), Trunc(b_G * bCR * 1.02), Trunc(b_B * bCR * 1.01));
    for nx := 0 to nx2 - 1 do
    begin

      yr := Math.Power((nx - nx2 div 2), 2) / (nHd - nJ * 2);
      nYR := Trunc((5 - Random(5)) * (1 + yr / 100));
      ny2 := Trunc(yr + dy + nYR);
      //if (ny>=0)and(ny<=nH+40) then
      //begin
      //      ViewBoard.Canvas.LineTo(nx,ny);
      //end;
      aColorx := aColor2;
      if (nJ mod 2) = 1 then
        aColorx := aColor0;
      if (arYs[nx] <= nH) then
        for nK := arYs[nx] to ny2 - 1 do
        begin
          ViewBoard.Canvas.Pixels[nx, nK] := aColorx;
        end;
      arYs[nx] := ny2;
    end;
    aColor0 := aColor1;
    aColor1 := aColor2;
  end;
  aColorx := aColor0;
  for nx := 0 to nx2 - 1 do
  begin
    ny2 := nH;
    for nK := arYs[nx] to ny2 - 1 do
    begin
      if (nK <= nK) then
        ViewBoard.Canvas.Pixels[nx, nK] := aColorx;
    end;
  end;

  aColor0 := cBackColor;
  aColor1 := aColor0;
  //ViewBoard.Canvas.Brush.color:=aColor1;
  //ViewBoard.Canvas.FillRect(ViewBoard.Canvas.ClipRect);
  //ViewBoard.Canvas.GradientFill(aRect,$004AD967,$004A99A7,gdHorizontal);
  //ViewBoard.Canvas.Ellipse(1,5,55,77);
  //nW:=ViewBoard.Width;
  //nH:=ViewBoard.Height;
  //ViewBoard.Canvas.Pen.color:=clBlack;
  nx2 := nW div 2;
  nHd := 20;
  //SetLength(arYs,nx2);
  for nx := 0 to nx2 - 1 do
    arYs[nx] := 0;
  Randomize;
  for nJ := 0 to 9 do
  begin
    nx := 0;
    //yr:= Math.Power((nx - nx2 div 2),2)/(nHd-nJ*2);
    dy := -100 + nJ * 50;
    //ny:=Trunc(yr+dy);
    //ViewBoard.Canvas.MoveTo(nx,ny);
    //aRGB:=ColorToRGB(aColor1);
    aRGB := longint(aColor1);
    b_B := (aRGB and $FF0000) shr 16;
    b_G := (aRGB and $FF00) shr 8;
    b_R := (aRGB and $FF);
    bCR := 1;//-Random(2);
    if (bCR = 0) then
      bCR := 1;
    aColor2 := RGBToColor(Trunc(b_R * bCR * 1.03), Trunc(b_G * bCR * 1.02), Trunc(b_B * bCR * 1.01));
    for nx := 0 to nx2 - 1 do
    begin

      yr := Math.Power((nx - nx2 div 2), 2) / (nHd - nJ * 2);
      nYR := Trunc((5 - Random(5)) * (1 + yr / 100));
      ny2 := Trunc(yr + dy + nYR);
      //if (ny>=0)and(ny<=nH+40) then
      //begin
      //      ViewBoard.Canvas.LineTo(nx,ny);
      //end;
      aColorx := aColor2;
      if (nJ mod 2) = 1 then
        aColorx := aColor0;
      if (arYs[nx] <= nH) then
        for nK := arYs[nx] to ny2 - 1 do
        begin
          ViewBoard.Canvas.Pixels[nx + nx2, nK] := aColorx;
        end;
      arYs[nx] := ny2;
    end;
    aColor0 := aColor1;
    aColor1 := aColor2;
  end;
  aColorx := aColor0;
  for nx := 0 to nx2 - 1 do
  begin
    ny2 := nH;
    for nK := arYs[nx] to ny2 - 1 do
    begin
      if (nK <= nK) then
        ViewBoard.Canvas.Pixels[nx + nx2, nK] := aColorx;
    end;
  end;

  PaintBoardGrid;
  ViewBoard.ShowHint:=False;
end;

procedure TFMain1.PaintBoardGrid;
var
  aRect: TRect;
  nH, nW, nx, ny, nx2, ny2: integer;
  nDL, nDx, nDy, nI, nJ, nDw, nDz: integer;
  rX: real;
  arNum: array[0..9] of string;
  aOldFontSize:Integer;
  aOldBrushStyle:TFPBrushStyle;
begin
  nW := ViewBoard.Width;
  nH := ViewBoard.Height;
  rX := nH / nW;
  if (rX > 11 / 10) then
  begin
    nDL := nW div 10;
  end
  else
  begin
    nDL := nH div 11;
  end;
  nDx := (nW - nDL * 8) div 2;
  if nDx < 0 then
    nDx := 0;
  nDy := (nH - nDL * 9) div 2;
  if nDy < 0 then
    nDy := 0;

  vnDL := nDL;  //间隔
  vnDx := nDx;  //起点
  vnDy := nDy;  //起点

  //Self.vGridXScales[0]:=vnDx;
  //Self.vGridYScales[0]:=vnDy;
  for nI := 0 to 8 do
  begin
    nx := (nI + 0) * nDL + nDx;
    Self.vGridXScales[nI]:=nx;
  end;
  for nJ := 0 to 9 do
  begin
    ny := (nJ + 0) * nDL + nDy;
    Self.vGridYScales[nJ]:=ny;
  end;

  //X:
  nI := 0;
  nx := (nI + 0) * nDL + nDx;
  ny := nDy;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ny2 := nDy + nDL * 9;
  ViewBoard.Canvas.LineTo(nx, ny2);
  nI := 8;
  nx := (nI + 0) * nDL + nDx;
  ny := nDy;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ny2 := nDy + nDL * 9;
  ViewBoard.Canvas.LineTo(nx, ny2);


  nI := 1;
  while nI < 8 do
  begin
    nx := (nI + 0) * nDL + nDx;
    ny := nDy;
    ViewBoard.Canvas.MoveTo(nx, ny);
    ny2 := nDy + nDL * 4;
    ViewBoard.Canvas.LineTo(nx, ny2);
    Inc(nI);
  end;
  nI := 1;
  while nI < 8 do
  begin
    nx := (nI + 0) * nDL + nDx;
    ny := nDy + nDL * 5;
    ViewBoard.Canvas.MoveTo(nx, ny);
    ny2 := nDy + nDL * 9;
    ViewBoard.Canvas.LineTo(nx, ny2);
    Inc(nI);
  end;
  //y
  nJ := 0;
  while nJ <= 9 do
  begin
    nx := nDx;
    ny := (nJ + 0) * nDL + nDy;
    ViewBoard.Canvas.MoveTo(nx, ny);
    nx2 := nDx + nDL * 8;
    ViewBoard.Canvas.LineTo(nx2, ny);
    Inc(nJ);
  end;

  //楚河汉界
  aOldFontSize:= ViewBoard.Canvas.Font.Size;
  aOldBrushStyle:=ViewBoard.Canvas.Brush.Style;
  ViewBoard.Canvas.Font.Size:=22;
  ViewBoard.Canvas.Brush.Style:=TFPBrushStyle.bsClear;
  ViewBoard.Canvas.Pen.Style:=psSolid;

  nI := 1;
  nJ := 4;
  nx := (nI + 0) * nDL + nDx + nDx div 4;
  ny := (nJ + 0) * nDL + nDy + nDy div 4;
  ViewBoard.Canvas.TextOut(nx,ny,'楚 河');
  nI := 5;
  nx := (nI + 0) * nDL + nDx ; //+ nDx div 4
  ViewBoard.Canvas.TextOut(nx,ny,'汉 界');

  ViewBoard.Canvas.Font.Size :=aOldFontSize;
  ViewBoard.Canvas.Brush.Style :=aOldBrushStyle;

  //炮位:
  nDw := 3;
  nDz := 6;
  nI := 1;
  nx := (nI + 0) * nDL + nDx;
  nJ := 2;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 7;
  nx := (nI + 0) * nDL + nDx;
  nJ := 2;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 1;
  nx := (nI + 0) * nDL + nDx;
  nJ := 7;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 7;
  nx := (nI + 0) * nDL + nDx;
  nJ := 7;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  //兵站:
  nDw := 3;
  nDz := 6;
  nI := 0;
  nx := (nI + 0) * nDL + nDx;
  nJ := 3;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);

  nI := 0;
  nx := (nI + 0) * nDL + nDx;
  nJ := 6;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);

  nI := 2;
  nx := (nI + 0) * nDL + nDx;
  nJ := 3;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 2;
  nx := (nI + 0) * nDL + nDx;
  nJ := 6;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);


  nI := 4;
  nx := (nI + 0) * nDL + nDx;
  nJ := 3;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 4;
  nx := (nI + 0) * nDL + nDx;
  nJ := 6;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);


  nI := 6;
  nx := (nI + 0) * nDL + nDx;
  nJ := 3;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 6;
  nx := (nI + 0) * nDL + nDx;
  nJ := 6;
  ny := (nJ + 0) * nDL + nDy;
  ViewBoard.Canvas.MoveTo(nx + nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx + nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx + nDw + nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx + nDw + nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx + nDw, ny + nDw + nDz);
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 8;
  nx := (nI + 0) * nDL + nDx;
  nJ := 3;
  ny := (nJ + 0) * nDL + nDy;
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  nI := 8;
  nx := (nI + 0) * nDL + nDx;
  nJ := 6;
  ny := (nJ + 0) * nDL + nDy;
  //左:
  ViewBoard.Canvas.MoveTo(nx - nDw, ny - nDw - nDz);
  ViewBoard.Canvas.LineTo(nx - nDw, ny - nDw);
  ViewBoard.Canvas.LineTo(nx - nDw - nDz, ny - nDw);

  ViewBoard.Canvas.MoveTo(nx - nDw - nDz, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw);
  ViewBoard.Canvas.LineTo(nx - nDw, ny + nDw + nDz);

  //士线
  nI := 4;
  nx := (nI + 0) * nDL + nDx;
  nJ := 1;
  ny := (nJ + 0) * nDL + nDy;
  nx2 := nx - nDL;
  ny2 := ny - nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  nx2 := nx + nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  ny2 := ny + nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  nx2 := nx - nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);

  nI := 4;
  nx := (nI + 0) * nDL + nDx;
  nJ := 8;
  ny := (nJ + 0) * nDL + nDy;
  nx2 := nx - nDL;
  ny2 := ny - nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  nx2 := nx + nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  ny2 := ny + nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);
  nx2 := nx - nDL;
  ViewBoard.Canvas.MoveTo(nx, ny);
  ViewBoard.Canvas.LineTo(nx2, ny2);

  //1--9
  nJ := 0;
  ny := (nJ + 0) * nDL + nDy - nDL + 4;
  for nI := 0 to 8 do
  begin
    nx := (nI + 0) * nDL + nDx;
    ViewBoard.Canvas.TextOut(nx, ny, IntToStr(nI + 1));
  end;

  nJ := 9;
  arNum[0] := '一';
  arNum[1] := '二';
  arNum[2] := '三';
  arNum[3] := '四';
  arNum[4] := '五';
  arNum[5] := '六';
  arNum[6] := '七';
  arNum[7] := '八';
  arNum[8] := '九';
  arNum[9] := '十';
  ny := (nJ + 0) * nDL + nDy + nDL div 2 + 4;
  for nI := 0 to 8 do
  begin
    nx := (nI + 0) * nDL + nDx - 5;
    ViewBoard.Canvas.TextOut(nx, ny, arNum[8 - nI]);
    //ViewBoard.Canvas.TextOut(nx,ny,'一');
  end;

end;

procedure TFMain1.ReDrawPcsLst;
var aVwPcs:TViewPiece;
  nIx,nIc:Integer;
begin
    nIC:=Self.vPcsLstB.Count;
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        aVwPcs.DLen:=Self.vnDL;
        aVwPcs.Inversion:=True;
        aVwPcs.PaintImg;
        aVwPcs.OnClick:=@ViewPcsBClick;
        aVwPcs.OnMouseEnter:=@ViewPcsBMouseEnter;
        aVwPcs.OnMouseLeave:=@ViewPcsBMouseLeave;
      end;
    end;
    nIC:=Self.vPcsLstR.Count;
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        aVwPcs.DLen:=Self.vnDL;
        aVwPcs.PaintImg;
        aVwPcs.OnClick:=@ViewPcsRClick;
        aVwPcs.OnMouseEnter:=@ViewPcsRMouseEnter;
        aVwPcs.OnMouseLeave:=@ViewPcsRMouseLeave;
      end;
    end;
    vViewSesor.DLen := vnDL;
    vViewSesor.PaintImg;
    //vViewSesor.Parent:=PanCeB;
    //vViewSesor.Visible:=True;
end;

procedure TFMain1.ReDrawViewBoard;
begin
  if Self.vIsReady then
  begin
    Memo1.Lines.Add('ReDrawViewBoard');
    Self.PaintBoard;
    ReDrawPcsLst;
  end;
end;

procedure TFMain1.ReSizeProc;
begin
  ViewBoard.Align:=alNone;
  ViewBoard.Width:=PanelM.Width;
  ViewBoard.Height:=PanelM.Height;
  ViewBoard.Align:=alClient;
  if (ViewBoard.Picture<>nil)and(ViewBoard.Picture.Bitmap<>nil) then
  begin
  ViewBoard.Picture.Bitmap.Width:=ViewBoard.Width;
  ViewBoard.Picture.Bitmap.Height:=ViewBoard.Height;
  end;
  ReDrawViewBoard;

end;

procedure TFMain1.PutPcsToBoxCells;
var aVwPcs:TViewPiece;
  nIx,nIc,nIy,x,y:Integer;
  cAry:array[0..15] of char;
  nEach:Integer;
  function findIdx(const c:char):Integer;
  var aIx:Integer;
  begin
    Result:=-1;
    for aIx:=0 to 15 do
    begin
      if cAry[aIx]=c then
      begin
        Result:=aIx;
        Break;
      end;
    end;

  end;

begin
    nIC:=16;
    for nIx:=0 to nIc-1 do
    begin
      cAry[nIx]:=OriPcsPosBC[nIx];
    end;
    nEach:=PanCeB.Width div Self.vnDL;

    nIC:=Self.vPcsLstB.Count;
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        nIy:=findIdx(aVwPcs.Code);
        if (nIy>=0) then
        begin
          cAry[nIy]:=#0;
          x:=PanCeB.Width - (nIy mod nEach)*aVwPcs.DLen - vnDL;
          y:=(nIy div nEach)*aVwPcs.DLen;
          aVwPcs.Parent:=PanCeB;
          aVwPcs.Left:=x;
          aVwPcs.Top :=y;
          aVwPcs.inL :=aVwPcs.Left;
          aVwPcs.inT :=aVwPcs.Top;
          aVwPcs.isOnBoard:=False;
        end;
      end;
    end;

    for nIx:=0 to nIc-1 do
    begin
      cAry[nIx]:=OriPcsPosRC[nIx];
    end;

    nIC:=Self.vPcsLstR.Count;
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        //aVwPcs.BringToFront;
        nIy:=findIdx(aVwPcs.Code);
        if (nIy>=0) then
        begin
          cAry[nIy]:=#0;
          x:=(nIy mod nEach)*aVwPcs.DLen;
          y:=(nIy div nEach + 1)*aVwPcs.DLen;
          aVwPcs.Parent:=PanCeR;
          aVwPcs.Left:=x;
          aVwPcs.Top :=PanCeR.Height - y;
          aVwPcs.inL :=aVwPcs.Left;
          aVwPcs.inT :=aVwPcs.Top;
          aVwPcs.isOnBoard:=False;
        end;
      end;
    end;

end;

procedure TFMain1.pShowBoard(aBoard: TBoard);
var aVwPcs:TViewPiece;
  nIx,nIc,nIy,x,y:Integer;
  cBIDAry:array[0..15] of ansistring;
  cRIDAry:array[0..15] of ansistring;
  aCode,aSide,aSub:AnsiChar;
  sAID:String;
  nSx,nSy:Integer;
  aPc,aPs:TPoint;
begin
    //PutPcsToCells;
  nIc:=vPcsLstB.Count;
  for nIx:=0 to nIc-1 do
  begin
    aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
    aVwPcs.isOnBoard:=False;
  end;
  nIc:=vPcsLstR.Count;
  for nIx:=0 to nIc-1 do
  begin
    aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
    aVwPcs.isOnBoard:=False;
  end;
    {
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstB.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        aVwPcs.Parent:=PanCeB;
        cBIDAry[nIx]:=aVwPcs.AID;
      end;
    end;
    for nIx:=0 to nIc-1 do
    begin
      aVwPcs:=TViewPiece(vPcsLstR.Objects[nIx]);
      if (aVwPcs<>nil) then
      begin
        aVwPcs.Parent:=PanCeR;
        cRIDAry[nIx]:=aVwPcs.AID;
      end;
    end;
    }
    for x:=0 to cXmax do begin
    	for y:=0 to cYmax do begin
    	  aCode := aBoard.points[x][y].code;
    	  aSub  := aBoard.points[x][y].sub;
          //Memo1.Lines.Add('points['+IntToStr(x)+']['+IntToStr(y)+']:'+aCode+aSub);
          if (aCode<>cEmpty) then
          begin
            aSide:=Piece.getSide(aCode);
            if (aSide=cSideBlk) then
            begin
              {sAID:='';
              for nIx:=0 to nIc-1 do
              begin
                if (AnsiLeftStr(cBIDAry[nIx],1)=aCode) then
                begin
                  sAID:=cBIDAry[nIx];
                  cBIDAry[nIx]:=' ';
                  Break;
                end;
              end;
              }
              sAID:=aCode+aSub;
              if (sAID>'') then
              begin
                //Memo1.Lines.Add('sAID:'+sAID);
                nIy:=vPcsLstB.IndexOf(sAID);
                if (nIy>=0) then
                begin
                  aVwPcs:=TViewPiece(vPcsLstB.Objects[nIy]);
                  if (aVwPcs<>nil) then
                  begin
                    aVwPcs.Parent:=PanelM;
                    nSx:=  (cXmax-x)*vnDL + vnDx - vnDL div 2;
                    nSy:=  (cYmax-y)*vnDL + vnDy - vnDL div 2;
                    aPc:=Point(nSx,nSy);
                    aPs:=  ViewBoard.ClientToScreen(aPc);
                    aPc:=  PanelM.ScreenToClient(aPs);
                    aVwPcs.Left := aPc.x;
                    aVwPcs.Top  := aPc.y;
                    aVwPcs.inI :=(cXmax-x);
                    aVwPcs.inJ :=(cYmax-y);
                    aVwPcs.isOnBoard:=True;
                  end;
                end;
              end;

            end else if (aSide=cSideRed) then
            begin
              {
              sAID:='';
              for nIx:=0 to nIc-1 do
              begin
                if (AnsiLeftStr(cRIDAry[nIx],1)=aCode) then
                begin
                  sAID:=cRIDAry[nIx];
                  cRIDAry[nIx]:=' ';
                  Break;
                end;
              end;
              }
              sAID:=aCode+aSub;
              if (sAID>'') then
              begin
                //Memo1.Lines.Add('sAID:'+sAID);
                nIy:=vPcsLstR.IndexOf(sAID);
                if (nIy>=0) then
                begin
                  aVwPcs:=TViewPiece(vPcsLstR.Objects[nIy]);
                  if (aVwPcs<>nil) then
                  begin
                    aVwPcs.Parent:=PanelM;
                    nSx:=  (cXmax-x)*vnDL + vnDx - vnDL div 2;
                    nSy:=  (cYmax-y)*vnDL + vnDy - vnDL div 2;
                    aPc:=Point(nSx,nSy);
                    aPs:=  ViewBoard.ClientToScreen(aPc);
                    aPc:=  PanelM.ScreenToClient(aPs);
                    aVwPcs.Left := aPc.x;
                    aVwPcs.Top  := aPc.y;
                    aVwPcs.inI :=(cXmax-x);
                    aVwPcs.inJ :=(cYmax-y);
                    aVwPcs.isOnBoard:=True;
                  end;
                end;
              end;

            end;

          end;
        end;
    end;


end;

procedure TFMain1.printBoardH(aBoard: TBoard);
var sL:string;
  x,y:Byte;
begin
  for y:= cYmax downto 0 do begin
  	sL :='';
        for x:= cXmax downto 0 do begin
  		sL := sL + aBoard.points[x][y].code;
  	end;
  	Memo1.Lines.Add(sL+'['+IntToStr(y)+']');
  end;
  sL:='';
  for x:= cXmax downto 0 do begin
  	sL := sL + IntToStr(x);
  end;
  Memo1.Lines.Add(sL+'[+]');

end;

procedure TFMain1.viewToBoard(aBoard: TBoard);
var nI,nJ,nX,nY:Byte;
    aIc,aIx:Integer;
    aVwPcs:TViewPiece;
    cP0,cP2:AnsiChar;
begin
  if aBoard = nil then Exit;
  TBoard.clearBoard(aBoard);
  aIc:=vPcsLstB.Count;
  for aIx:=0 to aIc-1 do
  begin
    aVwPcs:=TViewPiece(vPcsLstB.Objects[aIx]);
    if aVwPcs.isOnBoard then
    begin
      cP2:=aVwPcs.Code;
      nI:=aVwPcs.inI;
      nJ:=aVwPcs.inJ;
      nX:=cXmax-nI;
      nY:=cYmax-nJ;
      cP0:=aBoard.points[nX,nY].Code;
      if cP0 <> Board.cEmpty then
      begin
        raise Exception.Create('Points conflict! At X='+IntToStr(nX)+ ',Y='+IntToStr(nY)+',And the origin Piece is:'+cP0);
      end;
      aBoard.points[nX,nY].code:=cP2;
      aBoard.points[nX,nY].sub :=aVwPcs.AID[2];
    end;

  end;
  aIc:=vPcsLstR.Count;
  for aIx:=0 to aIc-1 do
  begin
    aVwPcs:=TViewPiece(vPcsLstR.Objects[aIx]);
    if aVwPcs.isOnBoard then
    begin
      cP2:=aVwPcs.Code;
      nI:=aVwPcs.inI;
      nJ:=aVwPcs.inJ;
      nX:=cXmax-nI;
      nY:=cYmax-nJ;
      cP0:=aBoard.points[nX,nY].Code;
      if cP0 <> Board.cEmpty then
      begin
        raise Exception.Create('Points conflict! At X='+IntToStr(nX)+ ',Y='+IntToStr(nY)+',And the origin Piece is:'+cP0);
      end;
      aBoard.points[nX,nY].code:=cP2;
      aBoard.points[nX,nY].sub :=aVwPcs.AID[2];
    end;

  end;

end;

end.
