unit ViewPiece;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,ExtCtrls,FPCanvas,Graphics
  , BGRABitmap, bgrabitmaptypes,LCLType
  , Piece;
type

  { TViewPiece }

  TViewPiece = class(TImage)
  private
    FAID: String;
    FCode: AnsiChar;
    FCName: String;
    FDLen: Integer;
    FinI: Byte;
    FinJ: Byte;
    FinL: Integer;
    FinT: Integer;
    FInversion: Boolean;
    FisOnBoard: Boolean;
    FSide: AnsiChar;
    vtmpBitmap: TBitmap;

  protected
  public
    property AID :String read FAID Write FAID;
    property Code :AnsiChar read FCode Write FCode;
    property CName :String read FCName Write FCName;
    property DLen :Integer read FDLen Write FDLen;
    property Inversion :Boolean read FInversion Write FInversion;
    property Side :AnsiChar read FSide Write FSide; //B/R
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PaintImg;
  public
    //on board info
    property inI :Byte read FinI Write FinI; //from left to right
    property inJ :Byte read FinJ Write FinJ; //from top to bottom
    property isOnBoard :Boolean read FisOnBoard Write FisOnBoard;
    //on Box info
    property inL :Integer read FinL Write FinL;
    property inT :Integer read FinT Write FinT;

  end;

  var cBackColor:TColor = $004FBF8F; //BGR
  var cFaceColor:TColor = $00E7F7E7; //BGR

implementation

{ TViewPiece }

constructor TViewPiece.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FCode:='E';
  FDLen:=64;
  FCName:='師';
end;

destructor TViewPiece.Destroy;
begin
  if Assigned(vtmpBitmap) then
    vtmpBitmap.free;
  inherited Destroy;
end;

procedure TViewPiece.PaintImg;
var
  aBmp: TBGRABitmap;
  Ox, Oy, Rx, Ry , Rxt, Ryt: single;
  aCa,aCs: TBGRAPixel;
  inEdg,OtEdg,xt,yt,txH,txW:Integer;
  siz1,sizx:Integer;
  sNam:string;
  aSwapC:TColor;
begin
  inEdg:=4;
  otEdg:=1;
  Self.Picture.Clear;
  Self.Width:=Self.DLen;
  Self.Height:=Self.DLen;
  Self.Transparent := False;
  Self.Canvas.Brush.Color := cBackColor;//clBlue;//clWhite;
  Self.Canvas.Brush.Style := TFPBrushStyle.bsSolid;
  Self.Canvas.FillRect(0, 0, Self.Width, Self.Height);

  aBmp := TBGRABitmap.Create(Self.Width, Self.Height, BGRA(0, 0, 0, 0));
  //aBmp.Canvas.Pen.Color:=clRed;
  //aBmp.Canvas.Pen.Width:=4;
  //aBmp.Canvas.AntialiasingMode:=amOn;
  Ox := (Self.Width - 1) / 2;
  Oy := (Self.Width - 1) / 2;
  Rx := (Self.Width - 1- otEdg*2) / 2;
  Ry := (Self.Height - 1- otEdg*2) / 2;
  aCa := ColorToBGRA(cFaceColor, 255); //BGRA(210,230,180,255)

  aBmp.FillEllipseAntialias(Ox, Oy, Rx, Ry, aCa);

  Rx := Trunc((Self.Width-1 - inEdg*2) / 2 );
  Ry := Trunc((Self.Height-1 - inEdg*2) / 2 );
  if Self.Side =cSideBlk then
  begin
    aCs:=BGRA(20, 20, 25, 255);
  end else
  begin
    aCs:=BGRA(255, 0, 0, 255);
  end;
  aBmp.EllipseAntialias(Ox, Oy, Rx, Ry, aCs, (inEdg-otEdg) );
  //Ellipse(7,7,Image2.Width-8,Image2.Height-8);
  aBmp.Draw(Self.Canvas, 0, 0, False);
  aBmp.Free;

  Self.Canvas.Font.CharSet := LCLType.GB2312_CHARSET;
  // = 134;//LCLType.FCS_ISO_10646_1 ;// = 4;  // Unicode;
  Self.Canvas.Font.Name := '楷体';
  Self.Canvas.Font.Color := aCs;
  Self.Canvas.Brush.Style := TFPBrushStyle.bsClear;
  Self.Canvas.Font.Style := [fsBold];
  Self.Canvas.Font.Quality:=fqProof;//fqCleartypeNatural;//fqProof;   fqCleartypeNatural  fqAntialiased  fqCleartype
  sNam:=Self.CName;
  siz1:=12;
  sizx:=siz1;
  txH:=0; txW:=0;
  Rxt:= Trunc(Rx);
  Ryt:= Trunc(Ry);
  //Memo1.Lines.Add('Rxt:'+FloatToStr(Rxt));
  //Memo1.Lines.Add('Ryt:'+FloatToStr(Ryt));
  while (sizx<78) do
  begin
    Self.Canvas.Font.Size := sizx;
    txW:=Self.Canvas.GetTextWidth(sNam);
    txH:=Self.Canvas.GetTextHeight(sNam);

    if (txW>=Rxt*2)or(txH>=Ryt*2) then
    begin
      if (sizx>19) then
      sizx:=sizx-2
      else
      sizx:=sizx-1;
      Self.Canvas.Font.Size := sizx;
      txW:=Self.Canvas.GetTextWidth(sNam);
      txH:=Self.Canvas.GetTextHeight(sNam);
      //Memo1.Lines.Add('Size:'+IntToStr(sizx)+',txW:'+IntToStr(txW)+',txH:'+IntToStr(txW));
      break;
    end;
    sizx:=sizx +1;
  end;
  xt:=Trunc(Rxt - txW / 2)+inEdg+0;
  yt:=Trunc(Ryt - txH / 2)+inEdg+0;
  //Memo1.Lines.Add('sNam:'+sNam+',xt:'+IntToStr(xt)+',yt:'+IntToStr(yt));
  Self.Canvas.TextOut(xt, yt, sNam);

  if (Inversion) then
  begin
    {
    vtmpBitmap := TBitmap.Create;
    vtmpBitmap.Transparent:=False;
    //vtmpBitmap.TransparentMode:=tmFixed;
    //vtmpBitmap.TransparentColor:=clBlue;
    vtmpBitmap.Width:=FDLen;
    vtmpBitmap.Height:=FDLen;
    vtmpBitmap.Canvas.Brush.Style:=TFPBrushStyle.bsSolid;
    //vtmpBitmap.Canvas.Brush.Color:=cBackColor;
    //vtmpBitmap.Canvas.FillRect(0,0,FDLen,FDLen);
    //vtmpBitmap.Canvas.Pen.Style:=TFPPenStyle.psSolid;
    //vtmpBitmap.Canvas.Pen.Color:=aCs;
    //vtmpBitmap.Canvas.Ellipse(0,0,FDLen-1,FDLen-1);
    }
    for xt := 0 to Self.Width - 1 do
    begin
      for yt := 0 to (Self.Height - 1) div 2 do
      begin
        aSwapC := Self.Canvas.Pixels[xt, Self.Height - 1 - yt];
        Self.Canvas.Pixels[xt, Self.Height - 1 - yt] := Self.Canvas.Pixels[xt, yt];
        Self.Canvas.Pixels[xt, yt]:=aSwapC;
      end;
      //若有中点，则不交换
    end;
    for yt := 0 to (Self.Height - 1) do
    begin
      for xt := 0 to (Self.Width - 1) div 2 do
      begin
      aSwapC := Self.Canvas.Pixels[(Self.Width - 1)-xt, yt];
      Self.Canvas.Pixels[(Self.Width - 1)-xt, yt] := Self.Canvas.Pixels[xt, yt];
      Self.Canvas.Pixels[xt, yt]:=aSwapC;
      end;
    end;
  end;
  Self.Picture.Bitmap.TransparentMode := tmFixed;
  Self.Picture.Bitmap.TransparentColor := Self.Canvas.Pixels[1, 1];
  Self.Transparent := True;

end;

end.

