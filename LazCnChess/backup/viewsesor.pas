unit viewSesor;

{$mode objfpc}{$H+}

interface
uses
  Classes, SysUtils,ExtCtrls,FPCanvas,Graphics
  ;


type

  { TViewSesor }

  TViewSesor = class(TImage)
  private

    FDLen: Integer;
    FSelected: Boolean;
    FSeleS: Char;
  protected
  public
    property Selected :Boolean read FSelected Write FSelected;
    property SeleID :string read FSeleID Write FSeleID;
    property SeleS :Char read FSeleS Write FSeleS;
    property DLen :Integer read FDLen Write FDLen;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure PaintImg;
  end;


implementation

{ TViewSesor }

constructor TViewSesor.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FDLen:=64;
end;

destructor TViewSesor.Destroy;
begin
  inherited Destroy;
end;

procedure TViewSesor.PaintImg;
var a,b,c:Integer;
  aPoints: array[0..3] of TPoint;
begin
  Self.Picture.Clear;
  Self.Width:=Self.DLen;
  Self.Height:=Self.DLen;
  Self.Transparent := False;
  Self.Canvas.Brush.Color := clBlack;
  Self.Canvas.Brush.Style := TFPBrushStyle.bsSolid;
  Self.Canvas.FillRect(0, 0, Self.Width, Self.Height);
  Self.Canvas.Brush.Color := $A0FEFE;
  Self.Canvas.Pen.Color := $402020;

  a:=Trunc((1.41*DLen - DLen)/2*0.70 );
  b:=Trunc((1.41*DLen - DLen)/2*0.70/2 );
  c:=DLen-1;

  aPoints[0].x:=0;
  aPoints[0].y:=0;
  aPoints[1].x:=0;
  aPoints[1].y:= b;
  aPoints[2].x:= a;
  aPoints[2].y:= a;
  aPoints[3].x:= b;
  aPoints[3].y:=0;
  Self.Canvas.Polygon(aPoints);

  aPoints[0].x:=c;
  aPoints[0].y:=0;
  aPoints[1].x:=c;
  aPoints[1].y:= b;
  aPoints[2].x:= c-a;
  aPoints[2].y:= a;
  aPoints[3].x:= c-b;
  aPoints[3].y:=0;
  Self.Canvas.Polygon(aPoints);

  aPoints[0].x:=c;
  aPoints[0].y:=c;
  aPoints[1].x:=c;
  aPoints[1].y:= c-b;
  aPoints[2].x:= c-a;
  aPoints[2].y:= c-a;
  aPoints[3].x:= c-b;
  aPoints[3].y:= c;
  Self.Canvas.Polygon(aPoints);

  aPoints[0].x:=0;
  aPoints[0].y:=c;
  aPoints[1].x:=0;
  aPoints[1].y:= c-b;
  aPoints[2].x:= a;
  aPoints[2].y:= c-a;
  aPoints[3].x:= b;
  aPoints[3].y:= c;
  Self.Canvas.Polygon(aPoints);

  Self.Picture.Bitmap.TransparentMode := tmFixed;
  Self.Picture.Bitmap.TransparentColor := clBlack;
  Self.Transparent := True;

end;

end.

