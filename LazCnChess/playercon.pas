unit PlayerCon;

{$mode objfpc}{$H+}

//玩家连接器基类，可派生为真人连接类PlayerHM及电脑连接类PlayerCPU
//传递游戏局面、状态、指令。
//以线程执行

interface

uses
  Classes, SysUtils;

type

  { TPlayerCon }

  TPlayerCon = class (TThread)

  public

    procedure Execute;override;

  end;

implementation

{ TPlayerCon }

procedure TPlayerCon.Execute;
begin

end;

end.

